#include <iostream>
#include <stdlib.h>
#include <vector>
#include <math.h>
#include <stdio.h>
#include "c_lib/rng.h"

#define NUM_REPS 100000
#define INTERVAL 0.07692307692307693 // 1/13

int roll() {
    /*
    To satisfy the constraints:
    P(1) = 1/13
    P(2) = 2/13
    P(3) = 2/13
    P(4) = 2/13
    P(5) = 2/13
    P(6) = 4/13

    Distributed between 0 and 1, the buckets are:
    0 - 1/13
    1/13 - 3/13
    3/13 - 5/13
    5/13 - 7/13
    7/13 - 9/13
    9/13 - 13/13
    */
    int d;
    double interval = 1/13;
    double rand = Random();
    //std::cout<<"rand="<<rand*100<<std::endl;
    if (rand < INTERVAL) {
        d = 1;
    } else if (rand < 3*INTERVAL) {
        d = 2;
    } else if (rand < 5*INTERVAL) {
        d = 3;
    } else if (rand < 7*INTERVAL) {
        d = 4;
    } else if (rand < 9*INTERVAL) {
        d = 5;
    } else {
        d = 6;
    }
    return d;    
}

int main() {
    double radius = 100;
    PutSeed(-1);
    std::cout<<"Starting simulation..."<<std::endl;
    int countSumEqualsSeven;
    int sum;
    for (int i = 0; i < NUM_REPS; i++) {
        int one = roll();
        int two = roll();
        sum = one + two;
        if (sum == 7) {
            //std::cout<<"sum == 7"<<std::endl;
            countSumEqualsSeven++;
        } else {
            //std::cout<<"sum = "<<sum<< " != 7"<<std::endl;
        }
    }
    double numReps = NUM_REPS;
    double probability = countSumEqualsSeven/numReps;
    printf ("p = %4.8f that the sum of two weighted dice rolls is 7.",probability);
}
