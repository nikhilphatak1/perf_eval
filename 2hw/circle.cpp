#include <iostream>
#include <stdlib.h>
#include <vector>
#include <math.h>
#include <stdio.h>
#include "c_lib/rng.h"

#define NUM_REPS 1000000

typedef struct Point {
    double x;
    double y;
} Point;

Point* makePoint(double theta, double radius) {
    //std::cout<<"making Point with theta = "<<theta<<std::endl;
    Point* p = new Point();
    p->x = radius*cos(theta);
    p->y = radius*sin(theta);
    return p;    
}

double distance(Point* a, Point* b) {
    double dx = a->x - b->x;
    double dy = a->y - b->y;
    return sqrt(dx*dx + dy*dy);
}

int main() {
    double radius = 100;
    PutSeed(-1);
    std::cout<<"Starting simulation..."<<std::endl;
    int countGreaterThanRadius = 0;
    double distanceHolder;
    for (int i = 0; i < NUM_REPS; i++) {
        Point* p = makePoint(2*M_PI*Random(), radius);
        Point* q = makePoint(2*M_PI*Random(), radius);
        distanceHolder = distance(p,q);
        if (distanceHolder > radius) {
            //std::cout<<"distance = "<<distanceHolder<< " was greater than radius = "<<radius<<std::endl;
            countGreaterThanRadius++;
        } else {
            //std::cout<<"distance = "<<distanceHolder<< " was not greater than radius = "<<radius<<std::endl;
        }
    }
    double numReps = NUM_REPS;
    double probability = countGreaterThanRadius/numReps;
    printf ("p = %4.8f that 2-point distance is greater than radius %f",probability,radius);
}
