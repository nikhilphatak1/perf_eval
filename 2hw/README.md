This file contains my answers to the written questions 
as well as instructions on how to run my code.

# Question 1

## Part A
The described functionality has been added. To compile and run:  

```
make ssq1
./ssq1
```

Results:  
   average interarrival time ............. = 9.87203800  
   average service time .................. = 7.12485100  
   average delay ......................... = 18.59399300  
   average wait .......................... = 25.71884400  
   proportion of delayed jobs ............ = 0.72300000  
   maximum delay ......................... = 118.76100000  
   number of jobs in service node at t= 400 =      7  

## Part B
The maximum delay was **118.7610 seconds**

## Part C 
**There are 7 jobs in the service node at t=400**
This is related to Theorem 1.2.1 (Proof of Little's Theorem) - this is the sum of the indicator functions for each job i at time=400.  This is equivalent to l(400), as shown in the proof.

## Part D
**The proportion of delayed jobs is 0.7230**

# Question 2

## Part A
To compile and run:

```
make ac
./ac
```
Results:  
**average service time ..... = 3.03189180**  
**utilization .............. = 0.73958805**  
**traffic intensity ........ = 0.74314549**  

## Part B
For each i, if job i arrives before job i-1 departs, **s_i = c_i - c_(i-1)** because job i cannot start being
serviced until job i-1 has completed.
In the other case, where job i arrives after job i-1 departs, **s_i = c_i - a_i**.
Finally, if a_i is less than a_(i-1), that means job i arrived before job i-1 and both of the above relations can be swapped i for i-1.  

# Question 3

## Part A
To compile and run:

```
make dice
./dice
```

Results:
Seed=1111
**p = 0.14061000**  
Seed=934459
**p = 0.14170000**  
Seed=-1 (system clock)
**p = 0.14165000**

## Part B
**The axiomatic probability is 24/169 = 0.1427**
We compute this by first postulating that the probability of rolling a 1
is 1/13.  This satisfies the constraints described in the problem as follows:
P(1) = 1/13
P(2) = 2/13
P(3) = 2/13
P(4) = 2/13
P(5) = 2/13
P(6) = 4/13

The possible ways to roll a 7 are:
(1,6),(6,1),(2,5),(5,2),(3,4),(4,3)

We compute the total probability as:
(1/13 * 4/13) * 2 + (2/13 * 2/13) * 2 + (2/13 * 2/13) * 2
= **24/169**  

# Question 4

## Part A
To compile and run:

```
make circle
./circle
```

Results:
Seed=2459
**p=0.66628900**  
Seed=8989
**p=0.66637200**
Seed=-1 (system clock)  
**p=0.66742800**

## Part B
**This probability does not depend on the radius**.  We can show this by fixing a point on the circle and picking the
second point randomly.  Only if the second point is within a pi/3 arc from the first point, will the chord length be less than
the radius, by equilateral triangle properties.  This totals to a 4*pi/3 arc of the circle where the chord length will be
longer than radius.  The proportion is (4*pi/3) / (2*pi) = 2/3.  Thus, p does not depend on the circle's radius.