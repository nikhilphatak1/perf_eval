
/*
Based on ssq1.c
 */

#include <stdio.h>                              

#define FILENAME   "ac.dat"                  /* input data file */
#define START      0.0

/* =========================== */
   double GetArrival(FILE *fp)                 /* read an arrival time */
/* =========================== */
{ 
  double a;

  fscanf(fp, "%lf", &a);
  return (a);
}

/* =========================== */
   double GetDeparture(FILE *fp)                 /* read a service time */
/* =========================== */
{ 
  double d;

  fscanf(fp, "%lf\n", &d);
  return (d);
}

/* ============== */
   int main(void)
/* ============== */
{
  FILE   *fp;                                  /* input data file      */
  long   index     = 0;                        /* job index            */
  double arrival   = START;                    /* arrival time         */
  double wait;                                 /* delay in queue       */
  double service;                              /* service time         */
  double departure = START;                    /* departure time       */
  double previousDeparture = 0;
  struct {                                     /* sum of ...           */
    double delay;                              /*   delay times        */
    double wait;                               /*   wait times         */
    double service;                            /*   service times      */
    double interarrival;                       /*   interarrival times */
    double delayedCount;                       /*   number of delays   */
  } sum = {0.0, 0.0, 0.0, 0.0, 0.0};
  double maximumDelay = 0;

  double timeToTrack = 400;

  fp = fopen(FILENAME, "r");
  if (fp == NULL) {
    fprintf(stderr, "Cannot open input file %s\n", FILENAME);
    return (1);
  }

  while (!feof(fp)) {
    arrival      = GetArrival(fp);
    departure    = GetDeparture(fp);
    if (index != 0) {
      if (arrival < previousDeparture) {
        sum.service += departure - previousDeparture;
      } else {
        sum.service += departure - arrival;
      }
    } else {
        sum.service += departure - arrival;
    }
    previousDeparture = departure;
    index++;
  }
  sum.interarrival = arrival - START;

  printf("\nfor %ld jobs\n", index);
  printf("   average service time ..... = %6.8f\n", sum.service / index);
  printf("   utilization .............. = %6.8f\n", sum.service / departure);
  printf("   traffic intensity ........ = %6.8f\n", sum.service / arrival);

  fclose(fp);
  return (0);
}
