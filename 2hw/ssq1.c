
/* -------------------------------------------------------------------------
 * This program simulates a single-server FIFO service node using arrival
 * times and service times read from a text file.  The server is assumed
 * to be idle when the first job arrives.  All jobs are processed completely
 * so that the server is again idle at the end of the simulation.   The
 * output statistics are the average interarrival time, average service
 * time, the average delay in the queue, and the average wait in the service 
 * node. 
 *
 * Name              : ssq1.c  (Single Server Queue, version 1)
 * Authors           : Steve Park & Dave Geyer
 * Language          : ANSI C
 * Latest Revision   : 9-01-98
 * Compile with      : gcc ssq1.c 
 * ------------------------------------------------------------------------- 
 */

#include <stdio.h>                              

#define FILENAME   "ssq1.dat"                  /* input data file */
#define START      0.0

/* =========================== */
   double GetArrival(FILE *fp)                 /* read an arrival time */
/* =========================== */
{ 
  double a;

  fscanf(fp, "%lf", &a);
  return (a);
}

/* =========================== */
   double GetService(FILE *fp)                 /* read a service time */
/* =========================== */
{ 
  double s;

  fscanf(fp, "%lf\n", &s);
  return (s);
}

/* ============== */
   int main(void)
/* ============== */
{
  FILE   *fp;                                  /* input data file      */
  long   index     = 0;                        /* job index            */
  double arrival   = START;                    /* arrival time         */
  double delay;                                /* delay in queue       */
  double service;                              /* service time         */
  double wait;                                 /* delay + service      */
  double departure = START;                    /* departure time       */
  double delayList[1000];
  struct {                                     /* sum of ...           */
    double delay;                              /*   delay times        */
    double wait;                               /*   wait times         */
    double service;                            /*   service times      */
    double interarrival;                       /*   interarrival times */
    double delayedCount;                       /*   number of delays   */
  } sum = {0.0, 0.0, 0.0, 0.0, 0.0};
  double maximumDelay = 0;

  double timeToTrack = 400;
  double numJobsTracked = 0;

  fp = fopen(FILENAME, "r");
  if (fp == NULL) {
    fprintf(stderr, "Cannot open input file %s\n", FILENAME);
    return (1);
  }

  while (!feof(fp)) {
    index++;
    arrival      = GetArrival(fp);
    printf("arrival=%6.2f ",arrival);
    if (arrival < departure) {
      delay      = departure - arrival;        /* delay in queue    */
      //delayList[index-1] = delay;
      sum.delayedCount++;
      if (delay > maximumDelay) {
        maximumDelay = delay;
      }
    }
    else {
      delay        = 0.0;                      /* no delay          */
      //delayList[index-1] = 0;
    }
    printf("delay=%6.2f ",delay);
    service      = GetService(fp);
    printf("service=%6.2f ",service);
    wait         = delay + service;
    departure    = arrival + wait;             /* time of departure */
    printf("departure=%6.2f\n",departure);
    sum.delay   += delay;
    sum.wait    += wait;
    sum.service += service;
    if (arrival <= timeToTrack && departure >= timeToTrack) {
      numJobsTracked++;
    }
  }
  sum.interarrival = arrival - START;

  printf("\nfor %ld jobs\n", index);
  printf("   average interarrival time ............. = %6.8f\n", sum.interarrival / index);
  printf("   average service time .................. = %6.8f\n", sum.service / index);
  printf("   average delay ......................... = %6.8f\n", sum.delay / index);
  printf("   average wait .......................... = %6.8f\n", sum.wait / index);
  printf("   proportion of delayed jobs ............ = %6.8f\n", sum.delayedCount / index);
  printf("   maximum delay ......................... = %6.8f\n", maximumDelay);
  printf("   number of jobs in service node at t=%4.0f = %6.0f\n", timeToTrack, numJobsTracked);
/*
  for (int i = 0; i < 1000; i++) {
    printf("%6.8f\n",delayList[i]);
  }*/

  fclose(fp);
  return (0);
}
