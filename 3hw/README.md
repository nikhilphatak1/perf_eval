This file contains my answers to the written questions 
as well as instructions on how to run my code.
This file is more easily legible at `https://gitlab.com/nikhilphatak1/perf_eval/-/blob/master/3hw/README.md`.

# Question 1

## Part A
Code change done. Compile and run with:

```
make ssq2
./ssq2
```

## Part B
- **average interarrival time**: approximately no change
- **average wait**: increases from 3.86 to 6.04
- **average delay**: increases, almost doubles from 2.36 to 4.53
- **average service time**: stays approximately no change
- **average number in the node**: increases from 1.91 to 3.02
- **average number in the queue**: increases, almost doubles from 1.17 to 2.27
- **utilization**: approximately no change


## Part C
- **average interarrival time**: Did not change because arrival times are not dependent on service times
- **average wait**: Increases because before, service times were constrained to between 1.0 and 2.0.  Now, service times have no upper bound.  Even if it is unlikely, a single very high service time can skew the average service time to be much higher, which is what happened in this example.  This value increases by about 56% - not as much as other values, because the exponential distribution also allows for very short service times which can slightly reduce the increase caused by high variance in service times.
- **average delay**: Delay is the difference between departure and arrival.  Because arrival times did not change, the change in average delay is completely dependent on the change in service time and how long jobs wait in the queue.  Because the service time variance is much higher, the average delay will increase accordingly. This change is about 92% from its original value because the delay is wait+service times. So if the queue becomes long OR if a job randomly gets a high service time, the overall delay is much higher on average.
- **average service time**: The new service time distribution is exponential, and still has a mean of 1.5 (just like a uniform distribution from 1.0 to 2.0).  Thus, this does not change.
- **average number in the node**: Because service times are more variable, we can have some jobs that are under service for a very large amount of time compared to with a uniform service time distribution.  Because the service policy is FIFO, there is no priority queueing to get short jobs serviced quickly.  However, the exponential distribution also allows for very short service times.  This is why this value increases by 58%, and not higher like delay or queue length.
- **average number in the queue**: The length of the queue depends on the service time for all jobs that arrived previously, after the last time the queue was empty.  Thus, a short sequence of high service time jobs will drastically skew the average queue length higher.  In this case, it increases by 94%.
- **utilization**: This value does not change significantly, primarily because the arrival times do not change when we change the service time distribution.  With the same mean service time, the server will be (generally) utilized and then idle for about the same amounts of time. Over the course of 100000 jobs, any variability gets flattened out.


# Question 2
Compile and run with:

```
make ssq2geom
./ssq2geom
```

Results:
average interarrival time =   2.00  
average wait ............ =   5.82  
average delay ........... =   4.32  
average service time .... =   1.50  
average # in the node ... =   2.91  
average # in the queue .. =   2.16  
utilization ............. =   0.75  

## Part A
The mean of the geometric distribution for the number of service tasks is `1 + geom(0.9)`, which is `1 + [0.9/(1-0.9)] = 10`.  Each task uses `Uniform(0.1,0.2)`, which has an average of `0.15`.  Multiplying these, we get a mean of `1.5` as expected.


## Part B
The run results above verify the above, and the run results are all within 0.3 of the results in 3.1.4.

## Part C
- **arrival rate**: no change because the arrival function is unrelated to the service node's timings.
- **average wait**: increases significantly because any instance where the number of tasks AND the uniform random task time for each of those tasks is above average, the mean gets skewed high disproportionately.
- **average delay**: increases because of the aforementioned higher variance in the service time - more opportunities for the queue size to increase.
- **mean service time**: stays the same as explained in Part A.
- **average number in the node**: increases because there is a possibility that several high service time events in a row may occur - this would result in enqueuing several events and the size of the queue growing.  This is not possible  if the service time is bounded to [1.0,2.0].
- **average number in the queue**: increases for the same reason as the average number of events in the service node above.
- **utilization**: stays the same because the same events are arriving at the same times - over a long period, the variance in service times will average out and the server will be *in use* for the same proportion of the time.


# Question 3

## Part A
Trivially, `P(X=0) = 1-beta` because that is the complement of the probability that a job feeds back once.  For a job to feedback once and then not again, we multiply the given feedback probability `beta` by its complement to get `P(X=1) = beta*(1-beta)`.  For a job to feedback twice and then not feedback, we similarly have `P(X=2) = beta*beta*(1-beta)`. In general, we can write:

```
P(X=x) = (1-beta)*(beta^x)
```

## Part B
This relates to the discussion of acceptance/rejection in section 2.3 as follows: in the circle probability distribution problem, we sample a square region in which the target circle is inscribed.  If the randomly selected point is in the target circle region we accept it, and reject otherwise.  The probability of acceptance is also the ratio of the area of the  circle to the area of the square around it.  In the same way, we can think of a job feeding back as rejection - we keep trying after a rejection, and the overall likelihood of several rejections in succession reduces exponentially with each rejection.


# Question 4

```
make ssq2finite
./ssq2finite
```

## Part A
Queue Capacity | Steady-State P(Rejection)  
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _  
      1        |            0.162  
      2        |            0.086  
      3        |            0.046  
      4        |            0.026  
      5        |            0.014  
      6        |            0.009  

## Part B
Changing service time to `Uniform(1.0,3.0)`:  

Queue Capacity | Steady-State P(Rejection)  
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _  
      1        |            0.259  
      2        |            0.180  
      3        |            0.139  
      4        |            0.109  
      5        |            0.091  
      6        |            0.077  

## Part C
The steady-state probability of rejection depends on how often there will be many jobs in the queue due to a series of jobs with high service times.  When the service time distribution is extended to `Uniform(1.0,3.0)`, there is a higher likelihood of a long service time, which also results in a high likelihood of a series of several long service times.

## Part D
I convinced myself that these tables are correct by adding print statements (now commented out) that provided my information about each job.  For example:

```
job # 94322
arrival = 188540.15
comparing arrival = 188540.15 and departure = 188538.72
decreasing queue size to   0.00
service =   1.72
departure = 188541.88
```

By looking at several jobs in sequence, I confirmed for myself that jobs in sequence resulted in enqueuing a job (or not) based on the arrival and service times.