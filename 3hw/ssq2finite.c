
/* ------------------------------------------------------------------------- 
 * This program - an extension of program ssq1.c - simulates a single-server 
 * FIFO service node using Exponentially distributed interarrival times and 
 * Uniformly distributed service times (i.e. a M/U/1 queue). 
 *
 * Name              : ssq2.c  (Single Server Queue, version 2)
 * Author            : Steve Park & Dave Geyer 
 * Language          : ANSI C 
 * Latest Revision   : 9-11-98
 * ------------------------------------------------------------------------- 
 */

#include <stdio.h>
#include <math.h>                   
#include <vector>                          
#include "rng.h"

#define LAST         100000L                   /* number of jobs processed */ 
#define START        0.0                      /* initial time             */ 
#define QUEUE        1                        /* finite queue capacity    */


   double Exponential(double m)                 
/* ---------------------------------------------------
 * generate an Exponential random variate, use m > 0.0 
 * ---------------------------------------------------
 */
{                                       
  return (-m * log(1.0 - Random()));     
}


   double Uniform(double a, double b)           
/* --------------------------------------------
 * generate a Uniform random variate, use a < b 
 * --------------------------------------------
 */
{                                         
  return (a + (b - a) * Random());    
}


   double GetArrival(void)
/* ------------------------------
 * generate the next arrival time
 * ------------------------------
 */ 
{       
  static double arrival = START;                                        

  arrival += Exponential(2.0);
  return (arrival);
}


   double GetService(void)
/* ------------------------------
 * generate the next service time
 * ------------------------------
 */ 
{
  return (Uniform(1.0, 3.0));
}


  int main(void)
{
  bool printJobs = false;
  long   index     = 0;                         /* job index            */
  double arrival   = START;                     /* time of arrival      */
  double delay;                                 /* delay in queue       */
  double service;                               /* service time         */
  double wait;                                  /* delay + service      */
  double departure = START;                     /* time of departure    */
  std::vector<double> departures;
  bool enqueue     = false;
  double queue     = 0;                         /* current queue size   */
  struct {                                      /* sum of ...           */
    double delay;                               /*   delay times        */
    double wait;                                /*   wait times         */
    double service;                             /*   service times      */
    double rejections;                          /*   rejection count    */
    double interarrival;                        /*   interarrival times */
  } sum = {0.0, 0.0, 0.0, 0.0};  

  PutSeed(123456789);

  while (index < LAST) {
    index++;
    if (printJobs) printf("\njob #%6.2ld\n",index);
    arrival      = GetArrival();
    if (printJobs) printf("arrival = %6.2f\n",arrival);
    std::vector<double>::iterator it = departures.begin();
    while (it != departures.end()) {
      double current = *it;
      if (printJobs) printf("comparing arrival = %6.2f and departure = %6.2f\n",arrival,current);
      if (arrival > current) {
        if (queue > 0) {
          queue--;
        }
        if (printJobs) printf("decreasing queue size to %6.2f\n",queue);
        it = departures.erase(it);
      } else {
        ++it;
      }
    }
    if (printJobs) printf("queue size = %6.2f\n",queue);
    if (queue == QUEUE+1) {
      sum.rejections++;
      if (printJobs) printf("queue is FULL - incremented # rejections = %6.2f\n",sum.rejections);
      continue;
    } else if (arrival < departure) {
      queue++;
      if (printJobs) printf("increasing queue size to %6.2f\n",queue);
      delay      = departure - arrival;         /* delay in queue    */
      enqueue = true;
    } else {
      delay      = 0.0;                         /* no delay          */
    }
    service      = GetService();
    if (printJobs) printf("service = %6.2f\n",service);
    wait         = delay + service;
    departure    = arrival + wait;              /* time of departure */
    if (printJobs) printf("departure = %6.2f\n",departure);
    if (enqueue) {
      departures.push_back(departure);
      if (printJobs) printf("storing departure\n");
      enqueue = false;
    }
    sum.delay   += delay;
    sum.wait    += wait;
    sum.service += service;
  } 
  sum.interarrival = arrival - START;

  printf("\nfor %ld jobs\n", index);
  printf("   average interarrival time = %6.4f\n", sum.interarrival / index);
  printf("   average wait ............ = %6.4f\n", sum.wait / index);
  printf("   average delay ........... = %6.4f\n", sum.delay / index);
  printf("   average service time .... = %6.4f\n", sum.service / index);
  printf("   average # in the node ... = %6.4f\n", sum.wait / departure);
  printf("   average # in the queue .. = %6.4f\n", sum.delay / departure);
  printf("   utilization ............. = %6.4f\n", sum.service / departure);
  printf("   probability of rejection  = %6.4f\n", sum.rejections / index);

  return (0);
}
