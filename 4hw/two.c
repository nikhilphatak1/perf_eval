/*
Based on ddh.c
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "rng.h"

#define DEFAULT_N_BALLS 10000
#define DEFAULT_N_BOXES 1000
#define BASE 10


int main(int argc, char *argv[]) {
    // parse args
    long nBalls, nBoxes;
    char *ptr;

    if (argc == 3) {
        nBalls = strtol(argv[1], &ptr, BASE);
        printf("Number of balls set to %ld\n", nBalls);

        nBoxes = strtol(argv[2], &ptr, BASE);
        printf("Number of boxes set to %ld\n", nBoxes);
    } else {
        nBalls = DEFAULT_N_BALLS;
        nBoxes = DEFAULT_N_BOXES;
        printf("Number of balls set to default %ld\n", nBalls);
        printf("Number of boxes set to default %ld\n", nBoxes);
    }

    // seed the rng
    PutSeed(43234);

    //pointer  head;   /* pointer to the head of the list */
    long counts[nBoxes];
    long hist[nBalls];
    for (int i = 0; i < nBoxes; i++) {
        counts[i] = 0;
    }

    for (int i = 0; i < nBalls; i++) {
        hist[i] = 0;
    }

    // for each ball, choose a box and increment the count for that box
    int nextRand;
    for (int i = 0; i < nBalls; i++)  {
        nextRand = floor(Random()*nBoxes);
        counts[nextRand]++;
    }

    // for each box, accumulate the counts for each box
    for (int i = 0; i < nBoxes; i++) {
        hist[counts[i]]++;
    }

    // look for the highest ball count for nice printing
    long highest_count = nBalls-1;
    for (int i = nBalls-1; i >= 0; i--) {
        if (hist[i] != 0) {
            highest_count = i+2; // i+1 is the first 0, need i+2 for half open interval in next for loop
            break;
        }
    }

    // for each count of balls, print out the count of boxes
    printf("     value      count   proportion\n\n");
    for (int i = 0; i < highest_count; i++) {
        printf("%10d %10ld %12.3f\n", i, hist[i], ((double)hist[i]) / ((double)nBoxes));
        //printf("%12.3f\n", ((double)hist[i]) / ((double)nBoxes));
    }
    printf("         ...\n");
    printf("         the rest of the counts are zero\n");
    
    // compute stats
    double xBar = 0;
    double sTemp = 0;
    for (int i = 0; i < nBalls; i++) {
        xBar += (double)i * (double)hist[i] / (double)nBoxes;
        sTemp += (double)i * (double)i * (double)hist[i] / (double)nBoxes;
    }
    double sDev = sqrt(sTemp - (xBar * xBar));

    printf("Stats:\n");
    printf("mean = %f\n", xBar);
    printf("std deviation = %f\n", sDev);

    return (0);
}
