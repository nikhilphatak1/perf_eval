#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "rng.h"

#define N_REPS 100000
#define N_TOTAL_QUESTIONS 120
#define N_TYPE_1_QUESTIONS 90
#define N_QUIZ_QUESTIONS 12
#define TRUE 1
#define FALSE 0
#define A 4
#define B 3
#define C 2
#define D 1
#define F 0

int typeOneScore() {
    double p = Random();
    if (p < 0.6) {
        // A
        return A;
    } else if (p < 0.9) {
        // B
        return B;
    } else {
        // C
        return C;
    }
}

int typeTwoScore() {
    double p = Random();
    if (p < 0.1) {
        // B
        return B;
    } else if (p < 0.5) {
        // C
        return C;
    } else if (p < 0.9) {
        // D
        return D;
    } else {
        // F
        return F;
    }
}

int contains(int arr[], int val) {
    for (int i = 0; i < N_QUIZ_QUESTIONS; i++) {
        if (arr[i] == val) {
            return TRUE;
        }
    }
    return FALSE;
}

int main(int argc, char *argv[]) {
    long results[N_REPS];
    long hist[A*N_QUIZ_QUESTIONS+1];
    int selectedQuestions[N_QUIZ_QUESTIONS];
    int score;
    PutSeed(1357531);

    for (long i = 0; i < N_REPS; i++) {
        score = 0;
        for (int j = 0; j < N_QUIZ_QUESTIONS; j++) {
            int qNum = (int)(Random() * N_TOTAL_QUESTIONS);
            while (contains(selectedQuestions, qNum)) {
                qNum = (int)(Random() * N_TOTAL_QUESTIONS);
            }
            selectedQuestions[j] = qNum;
        }

        for (int j = 0; j < N_QUIZ_QUESTIONS; j++) {
            if (selectedQuestions[j] < N_TYPE_1_QUESTIONS) {
                // type 1 question
                score += typeOneScore();
            } else {
                // type 2 question
                score += typeTwoScore();
            }
        }
        results[i] = score;
    }

    // construct the histogram

    for (int i = 0; i < A*N_QUIZ_QUESTIONS+1; i++) {
        hist[i] = 0;
    }

    // for each score, accumulate the counts for each score
    for (int i = 0; i < N_REPS; i++) {
        hist[results[i]]++;
    }
    // for each count of scores, print out the count of score samples
    printf("     value      count   proportion\n\n");
    for (int i = 0; i < A*N_QUIZ_QUESTIONS+1; i++) {
        printf("%10d %10ld %12.3f\n", i, hist[i], ((double)hist[i]) / ((double)N_REPS));
        //printf("%12.3f\n", ((double)hist[i]) / ((double)N_REPS));
    }

    long nPassed = 0;
    for (int i = A*N_QUIZ_QUESTIONS; i >= 36; i--) {
        nPassed += hist[i];
    }
    printf("Probability of passing = %f\n", ((double)nPassed)/((double)N_REPS));
}
