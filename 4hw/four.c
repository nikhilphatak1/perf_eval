#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "rng.h"

#define TRUE 1
#define FALSE 0
#define FILENAME "ac.dat"                  /* input data file */
#define START 0.0
#define N_JOBS 500
#define K 10.0

/* =========================== */
   double GetArrival(FILE *fp)                 /* read an arrival time */
/* =========================== */
{ 
  double a;

  fscanf(fp, "%lf", &a);
  return (a);
}

/* =========================== */
   double GetDeparture(FILE *fp)                 /* read a service time */
/* =========================== */
{ 
  double d;

  fscanf(fp, "%lf\n", &d);
  return (d);
}

double min(double arr[], int l) {
    double m = INFINITY;
    for (int i = 0; i < l; i++) {
        if (arr[i] < m) {
            m = arr[i];
        }
    }
    return m;
}

double max(double arr[], int l) {
    double m = -INFINITY;
    for (int i = 0; i < l; i++) {
        if (arr[i] > m) {
            m = arr[i];
        }
    }
    return m;
}


int main(int argc, char *argv[]) {
    /*  Code from HW2 for service times  */
    FILE   *fp;                                  /* input data file      */
    long   index     = 0;                        /* job index            */
    double arrival   = START;                    /* arrival time         */
    double wait;                                 /* delay in queue       */
    double service;                              /* service time         */
    double departure = START;                    /* departure time       */
    double previousDeparture = 0;
    struct {                                     /* sum of ...           */
        double delay;                              /*   delay times        */
        double wait;                               /*   wait times         */
        double service;                            /*   service times      */
        double interarrival;                       /*   interarrival times */
        double delayedCount;                       /*   number of delays   */
    } sum = {0.0, 0.0, 0.0, 0.0, 0.0};
    double maximumDelay = 0;

    double timeToTrack = 400;
    double serviceTimes[N_JOBS];

    fp = fopen(FILENAME, "r");
    if (fp == NULL) {
        fprintf(stderr, "Cannot open input file %s\n", FILENAME);
        return (1);
    }

    while (!feof(fp)) {
        arrival      = GetArrival(fp);
        departure    = GetDeparture(fp);
        if (index != 0) {
            if (arrival < previousDeparture) {
                sum.service += departure - previousDeparture;
                serviceTimes[index] = departure - previousDeparture;
            } else {
                sum.service += departure - arrival;
                serviceTimes[index] = departure - arrival;
            }
        } else {
            sum.service += departure - arrival;
            serviceTimes[index] = departure - arrival;
        }
        previousDeparture = departure;
        index++;
    }
    sum.interarrival = arrival - START;

    fclose(fp);


    // construct the histogram

    double a = 0; //min(serviceTimes, N_JOBS);
    double b = max(serviceTimes, N_JOBS)+5;
    double d;
    double k = (b-a)/d;
    int tmp;
    //int buckets[k];

    for (int i = 0; i < N_JOBS; i++) {
        tmp = (int) serviceTimes[i];
        //printf("%12.3f\n", serviceTimes[i]);
    }

    double sum_x = 0.0, mean, SD = 0.0;
    int i;
    for (i = 0; i < N_JOBS; ++i) {
        sum_x += serviceTimes[i];
    }
    mean = sum_x / N_JOBS;
    for (i = 0; i < N_JOBS; ++i)
        SD += pow(serviceTimes[i] - mean, 2);
    printf("stddev = %12.3f\nmean=%12.3f\n", sqrt(SD / N_JOBS), mean);

    /*
    // for each score, accumulate the counts for each score
    for (int i = 0; i < N_REPS; i++) {
        hist[results[i]]++;
    }
    // for each count of scores, print out the count of score samples
    printf("     value      count   proportion\n\n");
    for (int i = 0; i < A*N_QUIZ_QUESTIONS+1; i++) {
        printf("%10d %10ld %12.3f\n", i, hist[i], ((double)hist[i]) / ((double)N_REPS));
    }
    */
}
