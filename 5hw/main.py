from typing import List, Dict
import numpy as np




def main(S: List[int], V: List[float], K: int, N: int, names: List[str]):
    QLEN = np.zeros([K+1, N+1])
    RT = np.zeros([K+1, N+1])
    TPUT = np.zeros([N+1,])
    for i in range(1, N+1):
        for k in range(1, K+1):
            RT[k][i] = (1 + QLEN[k][i-1]) * S[k-1]
        denom = [RT[k][i]*V[k-1] for k in range(1, K+1)]
        TPUT[i] = float(i) / sum(denom)
        for k in range(1, K+1):
            QLEN[k][i] = V[k-1]*TPUT[i]*RT[k][i]

    print(f"System throughput = {TPUT[-1]:.6f}")
    for k in range(1, K+1):
        print(f"\"{names[k-1]}\" average queue length = {QLEN[k][-1]:.6f}")


if __name__ == "__main__":
    S = []
    V = []
    K = int(input("Number of devices: "))
    N = int(input("Number of customers: "))
    names = []
    print("Enter mean service time followed by visit ratio for each device:")
    for x in range(K):
        term = input().split()
        S.append(int(term[0]))
        V.append(float(term[1]))
        names.append(term[2])
    main(S, V, K, N, names)