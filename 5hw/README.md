# Homework 5
## Nikhil Phatak

### Instructions for Running
Activate the virtualenv:

`source ./venv/bin/activate`

`input.txt` contains the program input for the system on the homework, but you can run whatever system you want.  To run using the values in `input.txt`, in bash run:

`cat input.txt | python3 main.py`